import spacy
from spacy.matcher import Matcher
import syllapy
import random
import json

nlp = spacy.load("en_core_web_sm")
nlp.max_length = 1040000
matcher2 = Matcher(nlp.vocab)
matcher3 = Matcher(nlp.vocab)
matcher4 = Matcher(nlp.vocab)

pattern = [{'POS':  {"IN": ["NOUN", "ADP", "ADJ", "ADV"]} },
           {'POS':  {"IN": ["NOUN", "VERB"]} }]
matcher2.add("TwoWords", [pattern])
pattern = [{'POS':  {"IN": ["NOUN", "ADP", "ADJ", "ADV"]} },
           {'IS_ASCII': True, 'IS_PUNCT': False, 'IS_SPACE': False},
           {'POS':  {"IN": ["NOUN", "VERB", "ADJ", "ADV"]} }]
matcher3.add("ThreeWords", [pattern])
pattern = [{'POS':  {"IN": ["NOUN", "ADP", "ADJ", "ADV"]} },
           {'IS_ASCII': True, 'IS_PUNCT': False, 'IS_SPACE': False},
           {'IS_ASCII': True, 'IS_PUNCT': False, 'IS_SPACE': False},
           {'POS':  {"IN": ["NOUN", "VERB", "ADJ", "ADV"]} }]
matcher4.add("FourWords", [pattern])


filename = './data/source.txt'
doc = nlp(open(filename, 'r').read())

matches2 = matcher2(doc)
matches3 = matcher3(doc)
matches4 = matcher4(doc)

g_5 = []
g_7 = []

line1s = []
line2s = []
line3s = []


for match_id, start, end in matches2 + matches3 + matches4:
    string_id = nlp.vocab.strings[match_id]  # Get string representation
    span = doc[start:end]  # The matched span
    print('handling match_id {}'.format(match_id))
    syl_count = 0
    for token in span:
        syl_count += syllapy.count(token.text)
    if syl_count == 5:
        if random.random() > 0.5:
            if span.text not in line1s:
                line1s.append(span.text)
        else:
            if span.text not in line3s:
                line3s.append(span.text)
    if syl_count == 7:
        if span.text not in line2s:
            line2s.append(span.text)


keep = [[], [], []]

for i, lines in enumerate([line1s, line2s, line3s]):
    yes_count = 0
    random.shuffle(lines)
    for line in lines:
        line = line.lower()
        print('====\n{}\n===='.format(line))
        print('count: {}'.format(yes_count))
        response = input("Keep? \ny to keep \nx to exit \np to print \nanything else to continue\n")
        if response == 'p':
            print(keep)
            response = input("Keep? \ny to keep \nx to exit \nanything else to continue\n")
        if response == 'y':
            yes_count += 1
            keep[i].append(line)
        elif response == 'x':
            break
        elif response == 'p':
            print(keep)

filename = './data/final.json'
json.dump(fp=open(filename, 'w'), obj=keep, indent=4, separators=(',', ':'))
