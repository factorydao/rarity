import requests
from bs4 import BeautifulSoup

urls = [
    'https://en.wikipedia.org/wiki/Shiba_Inu',
    'https://en.wikipedia.org/wiki/Meat',
    'http://www.akirarabelais.com/i/i.html',
    'https://www.shibatoken.com/',
    'https://genekeys.com/articles/7-myths-about-money/',
    'https://genekeys.com/articles/the-great-change-entering-the-new-epoch/',
    'https://genekeys.com/articles/the-coming-storm-practical-guidance-for-thriving-during-the-21st-century/',
    'https://www.crystalinks.com/emerald1bw.html',
    'https://www.crystalinks.com/emerald2bw.html',
    'https://www.crystalinks.com/emerald3bw.html',
    'https://www.crystalinks.com/emerald4bw.html',
    'https://www.crystalinks.com/emerald5bw.html',
    'https://www.crystalinks.com/emerald6bw.html',
    'https://www.crystalinks.com/emerald7bw.html',
    'https://www.crystalinks.com/emerald8bw.html',
    'https://www.crystalinks.com/emerald9bw.html',
    'https://www.crystalinks.com/emerald10bw.html',
    'https://www.crystalinks.com/emerald11bw.html',
    'https://www.crystalinks.com/emerald12bw.html',
    'https://www.crystalinks.com/emerald13bw.html',
    'https://www.crystalinks.com/emerald14bw.html',
    'https://www.crystalinks.com/emerald15bw.html',
    'https://www.ancient-egypt-online.com/anubis.html',
    'http://fractalu.com/',
    'https://en.wikipedia.org/wiki/Game_mechanics',
    'https://en.wikipedia.org/wiki/Gambling',
    'https://www.lyricsfreak.com/d/dr+seuss/youre+a+mean+one+mr+grinch_21255165.html',
    'https://www.lyricsfreak.com/d/dr+seuss/fox+in+socks_20202887.html',
    'https://www.lyricsfreak.com/d/dr+seuss/green+eggs+and+ham_20208487.html',
    'https://www.pillsbury.com/everyday-eats/desserts/easy-dessert-recipes',
    'https://generatorfun.com/code/model/generatorcontent.php?recordtable=generator&recordkey=66&gen=Y&itemnumber=10&randomoption=undefined&genimage=Yes&nsfw=undefined&keyword=undefined&searchfilter=&tone=Normal&prefix=None',
    'https://generatorfun.com/code/model/generatorcontent.php?recordtable=generator&recordkey=66&gen=Y&itemnumber=10&randomoption=undefined&genimage=Yes&nsfw=undefined&keyword=undefined&searchfilter=&tone=Normal&prefix=None',
    'https://generatorfun.com/code/model/generatorcontent.php?recordtable=generator&recordkey=66&gen=Y&itemnumber=10&randomoption=undefined&genimage=Yes&nsfw=undefined&keyword=undefined&searchfilter=&tone=Normal&prefix=None',
    'https://generatorfun.com/code/model/generatorcontent.php?recordtable=generator&recordkey=66&gen=Y&itemnumber=10&randomoption=undefined&genimage=Yes&nsfw=undefined&keyword=undefined&searchfilter=&tone=Normal&prefix=None',
    'https://generatorfun.com/code/model/generatorcontent.php?recordtable=generator&recordkey=66&gen=Y&itemnumber=10&randomoption=undefined&genimage=Yes&nsfw=undefined&keyword=undefined&searchfilter=&tone=Normal&prefix=None',
    'https://blockgeeks.com/guides/what-is-cryptocurrency/',
    'https://www.forbes.com/sites/billybambrough/2021/07/25/dogecoin-is-money-elon-musk-shares-bullish-doge-theory-amid-200-billion-bitcoin-and-crypto-price-rally/',
    'https://www.poetryfoundation.org/poems/50826/the-dogs-at-live-oak-beach-santa-cruz',
    'https://www.poetryfoundation.org/poems/48065/mongrel-heart',
    'https://www.poetryfoundation.org/poems/54878/for-i-will-consider-your-dog-molly',
    'https://www.poetryfoundation.org/poems/43726/to-flush-my-dog',
    'https://www.poetryfoundation.org/poetrymagazine/poems/40632/if-feeling-isnt-in-it',
    'https://www.poetryfoundation.org/poems/39366/the-victor-dog',
    'https://www.poetryfoundation.org/poetrymagazine/poems/40659/dog-music',
    'https://www.poetryfoundation.org/poetrymagazine/poems/53952/unmediated-experience',
    'https://www.poetryfoundation.org/poems/49766/christmas-away-from-home',
    'https://www.poetryfoundation.org/poetrymagazine/poems/41145/over-and-over-tune'
]

filename = './data/source.txt'

with open(filename, 'w') as f:
    for url in urls:
        print('Pulling url {}'.format(url))
        text = requests.get(url).text
        soup = BeautifulSoup(text, 'html.parser')
        f.write(soup.get_text())