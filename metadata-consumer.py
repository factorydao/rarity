import pika
import time
import random
import json

def render_mock(ch, method, properties, body):
    time.sleep(random.randint(1, 4))
    config = json.loads(body)
    print('rendered {}'.format(config))


credentials = pika.PlainCredentials('guest', 'guest')
parameters = pika.ConnectionParameters('localhost', 5672, '/', credentials)
connection = pika.BlockingConnection(parameters)
# connection = pika.BlockingConnection(pika.ConnectionParameters('localhost'))

channel = connection.channel()
channel.queue_declare(queue='metadata', durable=True)
channel.basic_qos(prefetch_count=1)
channel.basic_consume(queue='metadata', auto_ack=True, on_message_callback=render_mock)
channel.start_consuming()
